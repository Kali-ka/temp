#include "Object.h"



Object::Object(const char * ObjFilePath, const std::string sourcePathVertexShader, const std::string sourcePathFramentShader) : Shader(sourcePathVertexShader, sourcePathFramentShader), Program(this->m_gShader[0], this->m_gShader[1])
{
	glGenVertexArrays(1, &m_VAO);
	glBindVertexArray(m_VAO);
	if (!Error::OpenGLErrorCheck())
	{
		printf("Object Creation!\n");
		return;
	}
	//PreTick(sourcePathVertexShader, sourcePathFramentShader);


	std::vector<glm::fvec4> vertices;
    std::vector<unsigned int> vertexIndices;
    std::vector<glm::fvec4> colors;
	meshLoader(ObjFilePath, vertices, vertexIndices ,colors, &this->m_rawVertices);

	if (InitVBO(GL_ARRAY_BUFFER, &vertices[0][0], vertices.size() * sizeof(glm::fvec4), 0, (void *)0, Object::VertexBufferDataType::VertexData) )
	{
		printf("InitVBO1 is OK!\n");
	}

	if (InitVBO(GL_ARRAY_BUFFER, &colors[0][0], colors.size() * sizeof(glm::fvec4), 1, (void *)0, Object::VertexBufferDataType::ColorData) )
	{
		printf("InitVBO2 is OK!\n");
	}

	if (InitVBO(GL_ELEMENT_ARRAY_BUFFER, &vertexIndices[0], vertexIndices.size() * sizeof(unsigned int)))
	{
		printf("InitVBO3 is OK!\n");
	}
}

/*
void Object::PreTick(const std::string sourcePathVertexShader, const std::string sourcePathFramentShader)
{

	m_vertexShader = Shader(sourcePathVertexShader, GL_VERTEX_SHADER);
	if (!m_vertexShader.CreateShader())
	{
		fprintf(stderr, "Error while creating the vertex shader\n");
		return;
	}
	m_fragmentShader = Shader(sourcePathFramentShader, GL_FRAGMENT_SHADER);
	if (!m_fragmentShader.CreateShader())
	{
		fprintf(stderr, "Error while creating the fragment shader\n");
		return;
	}

	m_Program = Program(&m_vertexShader, &m_fragmentShader);
	if (!m_Program.CreateProgram())
	{
		printf("Error in CreateProgram() method!\n");
		return;
	}
    
}
*/

bool Object::InitVBO(GLenum target, const float *data, GLsizeiptr DataSize, int layoutLocation, GLvoid * offset, VertexBufferDataType DataType)
{
	switch(DataType)
	{
		case VertexData:
		{
			glGenBuffers(1, &this->m_VBO_Vertex);
			glBindBuffer(target, this->m_VBO_Vertex);
		    glBufferData(target, DataSize, data, GL_STATIC_DRAW);
    		transforDataToShader(layoutLocation, offset);
    		break;
		}
		case ColorData:
		{
			glGenBuffers(1, &this->m_VBO_Color);
			glBindBuffer(target, this->m_VBO_Color);
		    glBufferData(target, DataSize, data, GL_STATIC_DRAW);
    		transforDataToShader(layoutLocation, offset);
    		break;
		}
	}
	if (!Error::OpenGLErrorCheck())
	{
		printf("Error while binding a VBO\n");
		return false;
	}
    glBindBuffer(target, 0);
    return true;
}

bool Object::InitVBO(GLenum target, const unsigned int *data, GLsizeiptr DataSize)
{
	glGenBuffers(1, &m_EBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBO);
	if (!Error::OpenGLErrorCheck())
	{
		printf("Error while binding a VBO\n");
		return false;
	}
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, DataSize, data, GL_STATIC_DRAW);
    if (!Error::OpenGLErrorCheck())
	{
		printf("Error while buffering a data\n");
		return false;
	}
    return true;
}

void Object::transforDataToShader(int layoutLocation, GLvoid * offset)
{
	glEnableVertexAttribArray(layoutLocation);
    glVertexAttribPointer(layoutLocation, 4, GL_FLOAT, GL_FALSE, 0, offset);
}

void Object::AABB(std::vector<glm::fvec4> &raw_vertices, glm::fvec3 &c_min, glm::fvec3 &c_max)
{
	c_min = c_max = glm::fvec3(raw_vertices[0]);
	for (int i = 1; i < raw_vertices.size(); ++i)
	{
		if (raw_vertices[i].x > c_max.x)
		{
			c_max.x = raw_vertices[i].x;
		}
		if (raw_vertices[i].x < c_min.x)
		{
			c_min.x = raw_vertices[i].x;
		}

		if (raw_vertices[i].y > c_max.y)
		{
			c_max.y = raw_vertices[i].y;
		}
		if (raw_vertices[i].y < c_min.y)
		{
			c_min.y = raw_vertices[i].y;
		}

		if (raw_vertices[i].z > c_max.z)
		{
			c_max.z = raw_vertices[i].z;
		}
		if (raw_vertices[i].z < c_min.z)
		{
			c_min.z = raw_vertices[i].z;
		}
	}

}

void Object::BoundingSphrere(glm::fvec3 &c_min, glm::fvec3 &c_max, glm::fvec3 &r_Centre, float &radius)
{
	r_Centre = (c_min + c_max)/2.0f; 
	radius = glm::l2Norm(c_max - c_min)/2.0f;
}

bool Object::meshLoader(const char * path, std::vector < glm::fvec4 > &out_vertices, std::vector<unsigned int> &vertexIndices, std::vector <glm::fvec4> &out_color, std::vector <glm::fvec4> * raw_vertices)
{
    std::vector< glm::fvec4 > temp_vertices, temp_color;
    FILE * file = fopen(path, "r");
    if( file == NULL ){ printf("Impossible to open the file !\n"); return false; }

    while(1)
    {
        char lineHeader[256];

        int res = fscanf(file, "%s", &lineHeader[0]);
        if (res == EOF)
            break;

        if ( strcmp( lineHeader, "v" ) == 0 )
        {
            glm::fvec4 vertex;
            fscanf(file, "%f %f %f %f\n", &vertex.x, &vertex.y, &vertex.z, &vertex.w);
            temp_vertices.push_back(vertex);
            raw_vertices->push_back(vertex);
        }
        else if ( strcmp( lineHeader, "c") == 0 )
        {
            glm::fvec4 color;
            fscanf(file, "%f %f %f %f\n", &color.x, &color.y, &color.z, &color.w);
            temp_color.push_back(color);
        }
        else if ( strcmp( lineHeader, "f" ) == 0 )
        {
            unsigned int vertexIndex[4], texIndex[4];
            int matches = fscanf(file, "%d/%d %d/%d %d/%d %d/%d\n", &vertexIndex[0], &texIndex[0], &vertexIndex[1], &texIndex[1], &vertexIndex[2], &texIndex[2],&vertexIndex[3], &texIndex[3]);
            if (matches != 8)
            {
                printf("File can't be read by our simple parser : ( Try exporting with other options\n");
                return false;
            }

            vertexIndices.push_back(vertexIndex[0]);
            vertexIndices.push_back(vertexIndex[1]);
            vertexIndices.push_back(vertexIndex[2]);

            vertexIndices.push_back(vertexIndex[2]);
            vertexIndices.push_back(vertexIndex[3]);
            vertexIndices.push_back(vertexIndex[0]);

        }

    }

    for( unsigned int i=0; i<vertexIndices.size(); i++ )
    {
        unsigned int vertexIndex = vertexIndices[i];
        glm::fvec4 vertex = temp_vertices[ vertexIndex-1 ];
        out_vertices.push_back(vertex);

        glm::fvec4 color = temp_color[ vertexIndex-1 ];
        out_color.push_back(color);
    }

    return true;
}

Object::~Object()
{
	glBindVertexArray(0);
}
