#include <vector>
#include <unistd.h>

#include "Game.h"
#include "Object.h"

Game::Game()
{
	if (!Init())
	{
		fprintf(stderr, "İnitialization error in Game class!\n");
		return;
	}
}

bool Game::Init()
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		Error::SDLErrorCheck();
		return false;
	}

	//Compatibility
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);

    m_Window = SDL_CreateWindow("Main", SCREEN_POS_X, SCREEN_POS_Y, SCREEN_W, SCREEN_H, SCREEN_FLAGS);
    if (m_Window == NULL)
    {
		Error::SDLErrorCheck();
		return false;
    }
    m_GLContext = SDL_GL_CreateContext(m_Window);
    SDL_GL_MakeCurrent(m_Window, m_GLContext);
    if (!Error::SDLErrorCheck())
    {
    	return false;
    }

    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK); // Set which face to cull
    glFrontFace(GL_CCW); // Set Front - Order matching

    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
    glDepthFunc(GL_LEQUAL);
    glDepthRange(-1.0f, 1.0f);

    printf("%s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
    printf("%s\n", glGetString(GL_VERSION));

    return true;
}

void Game::Tick()
{

    if (!Error::OpenGLErrorCheck())
    {
        printf("Before While\n");
        return;
    }

    while(!m_done)
    {
        if (!Error::OpenGLErrorCheck())
        {
            printf("In While GL\n");
            return;
        }

        if (!Error::SDLErrorCheck())
        {
            printf("In While SDL\n");
        }

        Render();
        EventTick();
    }

    if (!Error::SDLErrorCheck())
    {
        printf("After While\n");
        return;
    }
    if (!Error::OpenGLErrorCheck())
    {
        printf("After While\n");
        return;
    }
}

void Game::Render()
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClearDepth(1.0f);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glDrawArrays(GL_TRIANGLES, 0, 36);
    //glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);
    SDL_GL_SwapWindow(m_Window);
}

void Game::EventTick()
{
    SDL_Event s_GameEngEvent;
    while(SDL_PollEvent(&s_GameEngEvent))
    {
        switch(s_GameEngEvent.type)
        {
            case SDL_QUIT:
            {
                fprintf(stdout, "SQL is quitting...\n");
                m_done = true;
            }break;

            case SDL_KEYDOWN:
            {
                switch(s_GameEngEvent.key.keysym.scancode)
                {
                    case SDL_SCANCODE_ESCAPE:
                        {
                        fprintf(stdout, "SQL is quitting...\n");
                        m_done = true;
                        }break;
                    case SDL_SCANCODE_UP:
                        {
                            int x, y;
                            SDL_GetMouseState(&x,&y);
                            printf("Mouse State: %d %d\n", x, y);
                        }break;
                    case SDL_SCANCODE_DOWN:
                        {

                        }break;
                    default:
                    break;
                }
            }break;
        }
    }
}

Game::~Game()
{
	//Destructions...
    SDL_GL_DeleteContext(m_GLContext);
    SDL_DestroyWindow(m_Window);
    SDL_Quit();
}
