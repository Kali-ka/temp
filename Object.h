#pragma once

#include "stdafx.h"
#include "Shader.h"
#include "Program.h"

class Object : public Shader, public Program
{

private:
	Shader m_vertexShader; // Call constructor with input
	Shader m_fragmentShader;	// Call constructor with input

public:
	std::vector<glm::fvec4> m_rawVertices;

	GLuint m_VAO;
	GLuint m_VBO_Vertex;
	GLuint m_VBO_Color;
	GLuint m_VBO_Normal;
	GLuint m_EBO;

	Program m_Program;
	enum VertexBufferDataType
	{	
		VertexData, ColorData, IndexData
	};
	
public:
	Object(const char * path, const std::string sourcePathVertexShader, const std::string sourcePathFramentShader);
	void PreTick(const std::string sourcePathVertexShader, const std::string sourcePathFramentShader);
	bool InitVBO(GLenum target, const float *data, GLsizeiptr DataSize, int layoutLocation, GLvoid * offset, VertexBufferDataType DataType);
	bool InitVBO(GLenum target, const unsigned int *data, GLsizeiptr DataSize);
	void transforDataToShader(int layoutLocation, GLvoid * offset);
	void AABB(std::vector<glm::fvec4> &raw_vertices, glm::fvec3 &c_min, glm::fvec3 &c_max);
	void BoundingSphrere(glm::fvec3 &c_min, glm::fvec3 &c_max, glm::fvec3 &r_Centre, float &radius);
	bool meshLoader(const char * path, std::vector < glm::fvec4 > &out_vertices, std::vector<unsigned int> &vertexIndices, std::vector <glm::fvec4> &out_color, std::vector <glm::fvec4> * raw_vertices = new std::vector<glm::fvec4>);
	~Object();
	
};