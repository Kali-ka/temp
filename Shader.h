#pragma once

#include "stdafx.h"


#include <iostream>
#include <fstream>
#include <sstream>

class Shader 
{
private:
	const GLchar * vertexSource = R"glsl(
        #version 330
        layout(location = 0) in vec4 position;
        layout(location = 1) in vec4 color;

        uniform mat4 t_worldTranslate;
        uniform mat4 t_worldRotate;
        uniform mat4 viewTransformation;
        uniform mat4 projectiveTransformation;

        smooth out vec4 fragColor;
        void main()
        {
            mat4 worldTransformation = t_worldTranslate * t_worldRotate;
            gl_Position = projectiveTransformation * transpose(viewTransformation) * worldTransformation * position;
            fragColor = color;
        }
    )glsl";

    const GLchar * fragmentSource = R"glsl(
        #version 330 core
        smooth in vec4 fragColor;
        
        out vec4 outColor;
        
        void main()
        {
            outColor = fragColor;
        }
    )glsl";
	
public:
    std::vector<GLuint> m_gShader;
public:
	Shader();
	Shader(const std::string VertexSourcePath, const std::string FragmentSourcePath);
	bool CreateShader(const std::string SourcePath, GLenum shaderType);
	int LoadShaderSource(char* filename);
	unsigned long getFileLength(std::ifstream& file);
	bool ReadShaderSource(const std::string sourcePath, const GLchar * ShaderSourceContent);

    void WorldTransformation(glm::fvec3 tranlationVector, float rotationAngle);

	Shader(const Shader &obj);
	~Shader();
	
};
