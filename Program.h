#pragma once

#include "stdafx.h"
#include "Shader.h"

class Program
{
private:

public:
    GLuint m_shaderProgram;
public:
	Program();
	Program(GLuint vertexShader, GLuint fragmentShader);
	bool CreateProgram(GLuint vertexShader, GLuint fragmentShader);
	~Program();
	
};
