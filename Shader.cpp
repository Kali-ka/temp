#include "Shader.h"

Shader::Shader()
{

}

Shader::Shader(const std::string VertexSourcePath, const std::string FragmentSourcePath)
{
    CreateShader(VertexSourcePath, GL_VERTEX_SHADER);
    CreateShader(FragmentSourcePath, GL_FRAGMENT_SHADER);
}

bool Shader::CreateShader(const std::string SourcePath, GLenum shaderType) // use m_sourceFileName the file name
{
    const GLchar * ShaderSourceContent;
    GLuint shaderObject = glCreateShader(shaderType);
    if (!Error::OpenGLErrorCheck())
    {
        printf("asd1\n");
        return false;
    }
    /*
    if (!ReadShaderSource(m_sourceFileName))
    {
        fprintf(stderr, "Shader source file couldn't be read!\n");
        return false;
    }*/
    if (shaderType == GL_VERTEX_SHADER)
    {
        ShaderSourceContent = vertexSource;
    }
    else if(shaderType == GL_FRAGMENT_SHADER)
    {
        ShaderSourceContent = fragmentSource;
    }
    glShaderSource(shaderObject, 1, &ShaderSourceContent, NULL);

    if (!Error::OpenGLErrorCheck())
    {
        printf("asd2\n");
        return false;
    }

    glCompileShader(shaderObject);

    GLint status_vertex;
    glGetShaderiv(shaderObject, GL_COMPILE_STATUS, &status_vertex);
    if (status_vertex != GL_TRUE)
    {
        char buffer[512];
        glGetShaderInfoLog(shaderObject, 512, NULL, buffer);
        fprintf(stdout, "Shader Status: %s\n", buffer);
        return false;
    }

    this->m_gShader.push_back(shaderObject);
    return true;
}

bool Shader::ReadShaderSource(const std::string sourcePath, const GLchar * ShaderSourceContent)
{
    std::string content;
    std::ifstream f;
    f.open(sourcePath.c_str(), std::ios::in);

    if(!f.is_open()) {
        std::cerr << "Could not read file " << sourcePath << ". File does not exist." << std::endl;
        return false;
    }

    std::stringstream buffer;
    buffer << f.rdbuf();
    buffer << "\0";
    content = buffer.str();
    ShaderSourceContent = (GLchar *)content.c_str();

    f.close();
    return true;
}

void Shader::WorldTransformation(glm::fvec3 tranlationVector, float rotationAngle)
{

}

Shader::Shader(const Shader &obj)
{

}

Shader::~Shader()
{

}