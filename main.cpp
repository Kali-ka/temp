#include "Game.h"
#include "Object.h"

#include <math.h>

/*
	.._g**    : OpenGL object
*/

int main(int argc, char const *argv[])
{	
	Game testGame = Game();
    // /Volumes/Junction/Documents/Workplace/C++/OpenGL/Han/boxes/boxes.obj
    // /Volumes/Junction/Documents/Workplace/C++/OpenGL/Han/Crate/Crate1.obj
	Object testObj = Object("/Volumes/Junction/Documents/Workplace/C++/OpenGL/Han/Crate/Crate1.obj", "vertexShaderSource.txt", "fragmentShaderSource.txt");

    GLuint ID_worldTransformationTranslate = glGetUniformLocation(testObj.m_Program.m_shaderProgram, "t_worldTranslate");
    GLuint ID_worldTransformationRotate = glGetUniformLocation(testObj.m_Program.m_shaderProgram, "t_worldRotate");
    GLuint ID_viewTransformation = glGetUniformLocation(testObj.m_Program.m_shaderProgram, "viewTransformation");
    GLuint ID_projectiveTransformation = glGetUniformLocation(testObj.m_Program.m_shaderProgram, "projectiveTransformation");

    //---------------------World transformation---------------------//

    glm::mat4 w_TranslationMatrix = glm::mat4(1.0f);
    w_TranslationMatrix[3].x = 0.0f;
    w_TranslationMatrix[3].y = 0.0f;
    w_TranslationMatrix[3].z = -2.0f;

    const float theta = (M_PI/4);// + (M_PI/4);
    glm::mat4 w_RotationMatrix = glm::mat4(1.0f);
    w_RotationMatrix[0].x = cos(theta);
    w_RotationMatrix[2].x = sin(theta);
    w_RotationMatrix[0].z = -sin(theta);
    w_RotationMatrix[2].z = cos(theta);

    glUniformMatrix4fv(ID_worldTransformationTranslate, 1, GL_FALSE, glm::value_ptr(w_TranslationMatrix));
    glUniformMatrix4fv(ID_worldTransformationRotate, 1, GL_FALSE, glm::value_ptr(w_RotationMatrix));

    //---------------------View Transformation---------------------//
    const glm::fvec3 EYE = glm::fvec3(0.0f, 0.0f, 1.0f);
    const glm::fvec3 AT = glm::fvec3(0.0f, 0.0f, 0.0f);
    const glm::fvec3 UP = glm::fvec3(0.0f, 1.0f, 0.0f);

    glm::fvec3 ep_3 = ( EYE - AT )/( glm::l2Norm(EYE - AT) );
    glm::fvec3 ep_1 = ( cross(UP, ep_3) )/( glm::l2Norm( cross(UP, ep_3) ) );
    glm::fvec3 ep_2 = cross(ep_3, ep_1);

    glm::fvec4 t_ep_1 = glm::fvec4(ep_1, -dot(EYE, ep_1));
    glm::fvec4 t_ep_2 = glm::fvec4(ep_2, -dot(EYE, ep_2));
    glm::fvec4 t_ep_3 = glm::fvec4(ep_3, -dot(EYE, ep_3));

    glm::mat4 viewTransformation = glm::mat4(0.0f);
    viewTransformation[0] = t_ep_1;
    viewTransformation[1] = t_ep_2;
    viewTransformation[2] = t_ep_3;
    viewTransformation[3].w = 1.0f;
    glUniformMatrix4fv(ID_viewTransformation, 1, GL_FALSE, glm::value_ptr(viewTransformation));

    //---------------------Projective Transformation---------------------//

    glm::mat4 projectiveTransformation = glm::mat4(0.0f);

    const float fovy = 2.714;//2.214f;
 	const float aspect = 1.0f;
 	const float f = 10.0f;
 	const float n = 1.0f;

 	projectiveTransformation[0].x = (sin(fovy/2)/cos(fovy/2))/aspect;
 	projectiveTransformation[1].y = sin(fovy/2)/cos(fovy/2);
 	projectiveTransformation[2].z = -(f)/(f-n);
 	projectiveTransformation[3].z = -(n*f)/(f-n);
 	projectiveTransformation[2].w = -1;
 	glUniformMatrix4fv(ID_projectiveTransformation, 1, GL_FALSE, glm::value_ptr(projectiveTransformation));

 	//---------------------End of Transformations---------------------//
    
    glm::fvec3 c_min, c_max, r_Centre;
    float radius;
    testObj.AABB(testObj.m_rawVertices, c_min, c_max);
    testObj.BoundingSphrere(c_min, c_max, r_Centre, radius);
    printf("%f %f %f : %f %f %f\n", c_min[0], c_min[1], c_min[2], c_max[0], c_max[1], c_max[2]);
    printf("%f %f %f, r= %f\n", r_Centre[0], r_Centre[1], r_Centre[2], radius);

    printf("View Matrix: \n");
    for (int i = 0; i < 4; ++i)
    {
    	for (int j = 0; j < 4; ++j)
    	{
    		printf("%f ", viewTransformation[j][i]);
    	}
    	printf("\n");
    }

    printf("Projection Matrix: \n");
    for (int i = 0; i < 4; ++i)
    {
    	for (int j = 0; j < 4; ++j)
    	{
    		printf("%f ", projectiveTransformation[j][i]);
    	}
    	printf("\n");
    }

    testGame.Tick();
	return 0;
}
