#include "Program.h"


Program::Program()
{

}

Program::Program(GLuint vertexShader, GLuint fragmentShader)
{
    CreateProgram(vertexShader, fragmentShader);
}

bool Program::CreateProgram(GLuint vertexShader, GLuint fragmentShader)
{
	this->m_shaderProgram = glCreateProgram();
    if (!Error::OpenGLErrorCheck())
    {
        fprintf(stderr, "Error in the creation of program!\n");
        return false;
    }
	glAttachShader(m_shaderProgram, vertexShader);
    glAttachShader(m_shaderProgram, fragmentShader);
    if (!Error::OpenGLErrorCheck())
	{
		fprintf(stderr, "Error in the attaching of the shaders to the program!\n");
		return false;
	}

    glLinkProgram(m_shaderProgram);
    GLint program_linked;
    glGetProgramiv(m_shaderProgram, GL_LINK_STATUS, &program_linked);
    if (program_linked != GL_TRUE)
    {
        GLsizei log_length = 0;
        GLchar message[1024];
        glGetProgramInfoLog(m_shaderProgram, 1024, &log_length, message);
        fprintf(stdout, "%s\n", message);
        return false;
    }
    glUseProgram(m_shaderProgram);
	return true;
}

Program::~Program()
{
	
}
