#pragma once

//OpenGL headers
#define GL3_PROTOTYPES 1
#include <OpenGL/gl3.h>
#include <OpenGL/glext.h>

//SDL headers
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <SDL2/SDL_main.h>

//glm headers
#define _USE_MATH_DEFINES
#include <glm/glm.hpp>
#include <glm/gtc/type_precision.hpp>
#include <glm/gtx/norm.hpp>
#include <glm/gtc/type_ptr.hpp>

// C++ headers
#include <vector>
#include <string>

//Constants
#define SCREEN_H 600
#define SCREEN_W 600
#define SCREEN_POS_X 0
#define SCREEN_POS_Y 0
#define SCREEN_FLAGS SDL_WINDOW_SHOWN|SDL_WINDOW_INPUT_FOCUS|SDL_WINDOW_MOUSE_FOCUS|SDL_WINDOW_OPENGL

class Error
{
public:
	Error();
	inline static bool SDLErrorCheck()
	{
	    const char *error = SDL_GetError();
        if (*error) {
        	SDL_Log("SDL error: %s", error);
            fprintf(stderr, "SDL Error: %c\n", *error);
            SDL_ClearError();
            return false;
        }
	    return true;
	}

	inline static bool OpenGLErrorCheck()
	{
		{
			GLenum err;
			if ((err = glGetError()) != GL_NO_ERROR)
			{
				fprintf(stderr, "------------OpenGL Error (Begin)------------\n");
				fprintf(stderr, "glGetError Code: %u\n", err);
				const char * errString;
				errString = Error::GetGLErrorStr(err);
				fprintf(stderr, "glGetError String: %s\n", errString);
				fprintf(stderr, "------------OpenGL Error (End)------------\n");
				return false;
			}
		}
		return true;
	}
	
	inline static const char * GetGLErrorStr(GLenum err)
		{
		    switch (err)
		    {
		    case GL_NO_ERROR:          return "No error";
		    case GL_INVALID_ENUM:      return "Invalid enum";
		    case GL_INVALID_VALUE:     return "Invalid value";
		    case GL_INVALID_OPERATION: return "Invalid operation";
		    case GL_STACK_OVERFLOW:    return "Stack overflow";
		    case GL_STACK_UNDERFLOW:   return "Stack underflow";
		    case GL_OUT_OF_MEMORY:     return "Out of memory";
		    default:                   return "Unknown error";
		    }
	}
	~Error();
	
};
