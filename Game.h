#pragma once

#include "glm/glm.hpp"
#include <glm/gtc/type_precision.hpp>

#include "stdafx.h"

#include "Shader.h"
#include "Program.h"

class Game
{
private:
	SDL_Window * m_Window = NULL;
	SDL_GLContext m_GLContext = NULL;
	bool m_done = false;

public:
	

public:
	Game();
	bool Init();
	void Tick();
	void Render();
	void EventTick();
	void İnitProgram();
	~Game();

	inline static bool SDLErrorCheck()
	{
	    const char *error = SDL_GetError();
        if (*error) {
        	SDL_Log("SDL error: %s", error);
            fprintf(stderr, "SDL Error: %c\n", *error);
            SDL_ClearError();
            return false;
        }
	    return true;
	}

	inline static bool OpenGLErrorCheck()
	{
		{
			GLenum err;
			if ((err = glGetError()) != GL_NO_ERROR)
			{
				fprintf(stderr, "------------OpenGL Error (Begin)------------\n");
				fprintf(stderr, "glGetError Code: %u\n", err);
				const char * errString;
				errString = Game::GetGLErrorStr(err);
				fprintf(stderr, "glGetError String: %s\n", errString);
				fprintf(stderr, "------------OpenGL Error (End)------------\n");
				return false;
			}
		}
		return true;
	}
	
	inline static const char * GetGLErrorStr(GLenum err)
		{
		    switch (err)
		    {
		    case GL_NO_ERROR:          return "No error";
		    case GL_INVALID_ENUM:      return "Invalid enum";
		    case GL_INVALID_VALUE:     return "Invalid value";
		    case GL_INVALID_OPERATION: return "Invalid operation";
		    case GL_STACK_OVERFLOW:    return "Stack overflow";
		    case GL_STACK_UNDERFLOW:   return "Stack underflow";
		    case GL_OUT_OF_MEMORY:     return "Out of memory";
		    default:                   return "Unknown error";
		    }
	}
	
};
